package be.java2.kdg.dogscloud.bootstrap;

import be.java2.kdg.dogscloud.domain.Dog;
import be.java2.kdg.dogscloud.repository.DogRepository;
import be.java2.kdg.dogscloud.repository.MapDogRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class SeedData implements CommandLineRunner {
    private Logger logger = LoggerFactory.getLogger(SeedData.class);
    private DogRepository dogRepository;

    public SeedData(DogRepository dogRepository) {
        this.dogRepository = dogRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        logger.info("Seeding the repository with some dogs...");
        this.dogRepository.createDog(new Dog("Jack", Dog.Type.LABRADOR));
        this.dogRepository.createDog(new Dog("Max", Dog.Type.POODLE));
        this.dogRepository.createDog(new Dog("Linda", Dog.Type.SHEPPARD));
    }
}
