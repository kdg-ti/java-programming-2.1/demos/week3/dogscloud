package be.java2.kdg.dogscloud.service;

import be.java2.kdg.dogscloud.domain.Dog;
import be.java2.kdg.dogscloud.repository.DogRepository;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DogServiceImpl implements DogService{
    private DogRepository dogRepository;

    public DogServiceImpl(DogRepository dogRepository) {
        this.dogRepository = dogRepository;
    }

    @Override
    public List<Dog> getAllDogs() {
        return dogRepository.readDogs();
    }

    @Override
    public Dog addDog(String name, Dog.Type type) {
        Dog dog = new Dog(name, type);
        return dogRepository.createDog(dog);
    }
}
