package be.java2.kdg.dogscloud.service;

import be.java2.kdg.dogscloud.domain.Dog;

import java.util.List;

public interface DogService {
    List<Dog> getAllDogs();
    Dog addDog(String name, Dog.Type type);
}
