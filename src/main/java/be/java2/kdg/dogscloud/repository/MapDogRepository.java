package be.java2.kdg.dogscloud.repository;

import be.java2.kdg.dogscloud.domain.Dog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class MapDogRepository implements DogRepository{
    private Logger logger = LoggerFactory.getLogger(MapDogRepository.class);
    private Map<Integer, Dog> dogs = new HashMap<>();
    @Override
    public List<Dog> readDogs() {
        logger.debug("reading dogs");
        return new ArrayList<>(dogs.values());
    }

    @Override
    public Dog createDog(Dog dog) {
        logger.debug("creating a dog:" + dog);
        int maxId = dogs.values().stream().mapToInt(Dog::getId).max().orElse(0);
        dog.setId(maxId + 1);
        dogs.put(dog.getId(), dog);
        return dog;
    }
}
