package be.java2.kdg.dogscloud.repository;

import be.java2.kdg.dogscloud.domain.Dog;

import java.util.List;

public interface DogRepository {
    List<Dog> readDogs();
    Dog createDog(Dog dog);
}
