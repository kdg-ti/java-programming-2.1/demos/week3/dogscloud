package be.java2.kdg.dogscloud.presentation;


import be.java2.kdg.dogscloud.domain.Dog;
import be.java2.kdg.dogscloud.service.DogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/dogs")
public class DogController {
    private Logger logger = LoggerFactory.getLogger(DogController.class);
    private DogService dogService;

    public DogController(DogService dogService) {
        logger.debug("Creating dogController...");
        this.dogService = dogService;
    }

    @GetMapping
    public String showAllDogs(Model model){
        logger.debug("Running the showAllDogs method...");
        model.addAttribute("dogs", dogService.getAllDogs());
        model.addAttribute("name", "Johnny");
        return "dogs";
    }

    @GetMapping("/info")
    public String showDogInfo(Model model){
        logger.debug("Showing dogs info");
        //show the number of dogs in this doginfo page:
        model.addAttribute("numberOfDogs", dogService.getAllDogs().size());
        return "dogsinfo";
    }

    @GetMapping("/add")
    public String showAddDogForm(Model model){
        logger.debug("Showing adddogs form");
        //it needs the list of dog types!
        model.addAttribute("types", Dog.Type.values());
        return "adddog";
    }

    @PostMapping("/add")
    public String handleAddDog(Dog dog){
        logger.debug("Adding a dog with name:" + dog.getName());
        dogService.addDog(dog.getName(), dog.getType());
        return "redirect:/dogs";
    }
}
