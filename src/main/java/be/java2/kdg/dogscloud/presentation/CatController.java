package be.java2.kdg.dogscloud.presentation;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/cats")
public class CatController {
    @GetMapping
    public String showCats(){
        return "cats";
    }
}
